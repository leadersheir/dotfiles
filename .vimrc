syntax on

set incsearch
set noerrorbells
set nocompatible " for Vundle
set nowrap
set nu
set rtp+=~/.vim/bundle/Vundle.vim " for Vundle
set shiftwidth=4
set smartcase
set smartindent
set tabstop=4 softtabstop=4

filetype off

call vundle#begin()
Plugin 'ctrlpvim/ctrlp.vim' " fuzzy file finder
Plugin 'JuliaEditorSupport/julia-vim' " julia syntax highlighter
Plugin 'preservim/NERDTree' " file tree explorer
Plugin 'VundleVim/Vundle.vim' " plugin manager
call vundle#end()

filetype plugin indent on

" NERDTree Keymaps
nnoremap <C-b> :NERDTreeToggle<CR>
nnoremap <leader>n :NERDTreeFocus<CR>

" general key remaps
inoremap jk <Esc>
let mapleader=","

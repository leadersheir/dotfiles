# Path to your oh-my-zsh installation.
export ZSH="/home/cne/.oh-my-zsh"

# Set theme
ZSH_THEME="agnoster"

# Case insensitive completion
CASE_SENSITIVE="false"

# hyphen sensitive completion
HYPHEN_INSENSITIVE="false"

# disable auto-update checks
DISABLE_AUTO_UPDATE="true"

# Auto-correct
ENABLE_CORRECTION="false"

plugins=(git)

# aliases
alias klar="clear"
alias zshrc="sudo vim ~/.zshrc"
alias vimrc="sudo vim ~/.vimrc"

# function scripts

# backup/restore dotfiles
dotfiles() {
	if ! [[ -n "$1" ]]; then
		echo "use dotfiles [backup | restore]"
	elif [ "$1" = "backup" ]; then
		echo backing up dotfiles; echo
		cd; ./dev/bash-scripts/backup_dotfiles.sh
	elif [ "$1" = "restore" ]; then
		echo restoring dotfiles; echo
		cd; ./dev/bash-scripts/restore_dotfiles.sh
	else
		echo "invalid argument. use dotfiles [backup | restore]"
	fi
}

# setup new latex project boilerplate
tex() {
	project_name="$1"
	cd /home/cne/texnotes
	cat tex_boilerplate.txt > tex_boilerplate_temp.txt
	rpl TITLE $project_name tex_boilerplate_temp.txt
	mkdir $project_name
	touch $project_name/main.tex
	cat tex_boilerplate_temp.txt > $project_name/main.tex
	rm tex_boilerplate_temp.txt
	atom $project_name $project_name/main.tex
}

source $ZSH/oh-my-zsh.sh

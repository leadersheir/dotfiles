echo Updating and upgrading the system ...;
echo; echo;

sudo apt update && sudo apt full-upgrade -y &&

echo; echo;

echo System updated and upgraded successfully ...;

echo; echo;

echo Installing Brave browser;

echo; echo;

sudo apt install apt-transport-https curl && sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg && echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list sudo apt update && sudo apt install brave-browser -y &&

echo; echo;

echo Brave browser successfully installed. Removing firefox now;

echo; echo;

sudo apt remove firefox -y &&

echo; echo;

echo firefox successfully removed

echo; echo;

echo Installing the following softwares now
echo atom (text editor)
echo calibre (ebook manager)
echo caprine (facebook messenger)
echo joplin (notebook)
echo manim (python math animation library)
echo sagemath (CAS)
echo standard notes (simple notebook)
echo timeshift (system backup manager)
echo vim (text editor)
echo virtualbox (vm manager)
echo vlc (media player)
echo; echo;

echo Installing atom and packages

echo; echo;

sudo apt install atom -y &&
	apm install latex &&
	apm install latextools &&
	apm install pdf-view &&
	apm install language-julia &&
	apm install atom-django &&
	apm install react;

echo atom and packages installed successfully

echo; echo;

echo installing vim, vundle, and updating ~/.vimrc

sudo apt install vim -y &&
	sudo cat dotfiles/.vimrc > ~/.vimrc &&
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim &&
	vim +PluginInstall +qalla &&

echo; echo;

echo vim and vundle installed, vim plugins installed, ~/.vimrc updated successfully

echo; echo;

echo installing softwares
echo; echo;

sudo apt install calibre joplin sagemath sagemath-jupyter timeshift virtualbox vlc youtube-dl -y;

sudo apt install snapd -y && sudo snap install caprine standard-notes -y;

sudo apt install libcairo2-dev libpango1.0-dev ffmpeg texlive-full python3-pip -y &&
	sudo pip3 install numpy pandas scikit-learn jupyter manim;

echo; echo;

echo Installing Julia language;

echo; echo;

cd ~/Downloads &&
	wget https://julialang-s3.julialang.org/bin/linux/x64/1.7/julia-1.7.0-linux-x86_64.tar.gz &&
	tar -xvzf julia-1.7.0-linux-x86_64.tar.gz &&
	sudo cp -r julia-1.7.0 /opt/ &&
	echo "using Pkg\nPkg.add("IJulia")\nPkg.add("LinearAlgebra")" > temp.jl &&
	sudo julia temp.jl &&
	cd; rm Downloads/* -y;

echo; echo;

echo Julia language successfully installed along with packages IJulia and LinearAlgebra;

echo; echo;

echo Installing Node Version Manager (nvm) and node

echo; echo;

sudo curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | zsh &&
	sudo nvm install node &&
	sudo apt install yarn -y;

echo; echo;

echo Node version manager, node, and yarn package manager have been successfully installed

echo; echo;


